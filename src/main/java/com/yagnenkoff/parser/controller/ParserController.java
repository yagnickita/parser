package com.yagnenkoff.parser.controller;

import com.yagnenkoff.parser.domain.Processor;
import com.yagnenkoff.parser.service.ProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ParserController {
    private final ProcessorService processorService;

    @Autowired
    public ParserController(ProcessorService processorService) {
        this.processorService = processorService;
    }

    @RequestMapping(path = "/processors/update", method = RequestMethod.PUT)
    public void updateProcessors() {
        processorService.processorParse();
    }

    @RequestMapping(path = "/processors/all", method = RequestMethod.GET)
    public List<Processor> listProcessors() {
        return processorService.getProcessors();
    }
}
