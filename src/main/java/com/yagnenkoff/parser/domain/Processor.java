package com.yagnenkoff.parser.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@ToString
public class Processor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NonNull
    private String source;

    @NonNull
    private String model;

    @NonNull
    private String socket;

    @NonNull
    private String nCores;

    @NonNull
    private String frequency;

    public Processor() {
    }

    public Processor(@NonNull String source, @NonNull String model, @NonNull String socket, @NonNull String nCores, @NonNull String frequency) {
        this.source = source;
        this.model = model;
        this.socket = socket;
        this.nCores = nCores;
        this.frequency = frequency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSocket() {
        return socket;
    }

    public void setSocket(String socket) {
        this.socket = socket;
    }

    public String getnCores() {
        return nCores;
    }

    public void setnCores(String nCores) {
        this.nCores = nCores;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }
}
