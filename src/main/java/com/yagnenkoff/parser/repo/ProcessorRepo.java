package com.yagnenkoff.parser.repo;

import com.yagnenkoff.parser.domain.Processor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessorRepo extends JpaRepository<Processor, Long> {
    Processor findBySource(String source);
}
