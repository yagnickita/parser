package com.yagnenkoff.parser.service;


import com.yagnenkoff.parser.domain.Processor;

import java.util.List;

public interface ProcessorService {
    void processorParse();

    List<Processor> getProcessors();
}
