package com.yagnenkoff.parser.service.impl;

import com.yagnenkoff.parser.domain.Processor;
import com.yagnenkoff.parser.repo.ProcessorRepo;
import com.yagnenkoff.parser.service.ProcessorService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ProcessorServiceImpl implements ProcessorService {

    private final ProcessorRepo processorRepo;

    @Autowired
    public ProcessorServiceImpl(ProcessorRepo processorRepo) {
        this.processorRepo = processorRepo;
    }

    @Override
    public void processorParse() {
        Document doc;
        Document docProc;

        Processor proc;

        List<Processor> processors = new LinkedList<>();
        int page = 1;
        while (true) {
                try {
                    doc = Jsoup.connect("https://www.dns-shop.ru/catalog/17a899cd16404e77/processory/?p=" + page + "&order=1&groupBy=none&stock=2").get();
                    Elements items = doc.getElementsByClass("show-popover");
                    if (items.size() == 0) {
                        break;
                    }
                    for (Element item : items) {
                        docProc = Jsoup.connect("https://www.dns-shop.ru" + item.attr("href")).get();
                        String model = getCharacteristic("20102", docProc);
                        String socket = getCharacteristic("9094", docProc);
                        String nCores = getCharacteristic("816", docProc);
                        String frequency = getCharacteristic("817", docProc);
                        proc = new Processor(
                                "https://www.dns-shop.ru" + item.attr("href"),
                                model,
                                socket,
                                nCores,
                                frequency
                        );
                        System.out.println(proc);
                        processors.add(proc);
                    }
                    page++;
                } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (Processor processor : processors ) {
            if (processorRepo.findBySource(processor.getSource()) == null) {
                processorRepo.save(processor);
            }
        }
    }

    @Override
    public List<Processor> getProcessors() {
        return processorRepo.findAll();
    }

    private String getCharacteristic(String n, Document docProc) {
        return docProc
                .getElementsByAttributeValue("data-descr", n)
                .first()
                .parent()
                .parent()
                .parent()
                .parent()
                .children()
                .last()
                .text();
    }
}
